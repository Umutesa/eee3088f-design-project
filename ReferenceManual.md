# **Smart Traffic Robot System with Voice Applications**


**REFERENCE MANUAL FOR PCB BRIEF**

![alt text](https://yic-assm.com/wp-content/uploads/2017/09/xray-pcb.jpg)

**Table of Contents**

Introduction Overview	<br />
PURPOSE	<br />
Requirements	<br />
Mechanical Specifications	<br />
Detailed Mechanical Specifications with Testing	<br />
Electrical Specifications	<br />
Detailed Electrical Specification with Testing <br />
Functional Specifications with Testing<br />
Footprints Selection	<br />
Datasheet	<br />
555 Timer	<br />
IC	<br />
Universal Op Amp	<br />
BJT Transistor	<br />
Reference	<br />
	

# **Introduction Overview**

This PCB reference manual is intended to help one understand the design better by studying its functionalities.The model described in this manual is intended to be used in the PCB layout of KiCad
 
# **PURPOSE**
This document is the User manual reference guide
It is intended to provide all the necessary information to use this model  to develop real life existing applications.:
Specifies factual Facts on Equipments
 
  
# **Requirements**

**Users Requirements** <br />

**Pedestrians**

Project his/her voice to the smart traffic robot <br />
Receive feedback from the system, hence green to Go or red to Stop. <br />
Ensure that there is safety when crossing the road <br />
Be aware of the time length available to cross the road safely, before the smart robot switches it’s LED to a different colour <br />
Device needs to eliminates all kinds of disturbance in the background <br />
The system operates for 24 hours, 7 days a week <br />
The system operates in all environment conditions <br />

**Drivers**
Receive feedback from the device, hence Red to Stop for pedestrian crossing. <br />

**Traffic Road Controller**
The traffic robot system supplies additional power or back up power in case of emergency <br />
Monitor operation and performance of the smart traffic systems and road crossings <br />
The LEDs lights and brightness are working correctly <br />
The pi hat board should have GPIO Connectors <br />
The dimensions of the pi hat should be in synchronous with the dimensions of the Raspberry pi zero board. <br />
The pi hat board must include mounting holes <br />
The board must provide additional slots or cut-offs for camera/sensor/audio mic insertion. <br />
Mounting holes should be plated <br />
The weight of the pi board must not be too heavy <br />
The board must provide additional slots or cut-offs for camera/sensor/audio mic insertion <br />


# **Mechanical Specifications**

![alt text](https://pi-plates.com/wp-content/uploads/2015/03/HATmech.png)

# **Detailed Mechanical Specifications with Testing**


**Specification(s)**

The dimensions of the pi hat should be in synchronous with the dimensions of the Raspberry pi zero board <br />
The dimension of the board is  65x30 mm. <br />
The dimensions of the radius corner is 3 mm. <br />

**Acceptance Test Criteria**
The board dimensions can be measured using a calliper and the radius corners of the board can be measured using a radius gauge. <br />The uncertainty of the instrument and reading should be taken into consideration for accurate measurements.

**Specification(s)**
The pi hat board must include mounting hole<br />
The board must include three mounting holes,which  two of the mounting are adjacent to the GPIO headers and four  in the corner.<br />
The mounting holes need to be connected to ground.

**Acceptance Test Criteria**
Implement test points in the design circuits where the board has been mounted. These test points must be connected to ground and allow the measuring of various components.<br />
The below figure illustrates a test point placed between two resistors, the test point ensures that the components are correctly implemented by measuring their values with respect to ground.<br />

**Specification(s)**
The board must provide additional slots or cut-offs for camera/sensor/audio mic insertion<br />
 The pi hat must support USB Audio Class (UAC 1.0).<br />
 A built in microphone such as a MP34DT01TR must be implemented.The device needs to be, low power, omnidirectional, digital MEMS microphone built with a capacitive sensing element and an IC interface.<br />
A 3.5 mm headphone slot built in.<br />
The sensor camera mode needs to be a 2592x1944 pixels resolution with a weight of 3g and a still resolution of 5 Megapixels.

**Acceptance Test Criteria**
The mics and audio can be tested using a voice recognition software, examples include Jasper or the Raspberry Pi Voice Recognition by Oscar Liang. This system captures the voice by the microphone and then converts it to text using Google Voice API. If the commands are associated with it, it will execute the necessary responses.<br />
To test whether the sound is working correctly, connect your Raspberry Pi to your computer, open the terminal command and use the “Speaker Test” command to test and configure the default sound for your device.

**Specification(s)**
Mounting holes should be plated<br />
 Plating slots should be drilled and milled to 2.75mm length in diameter.<br />

**Acceptance Test Criteria**
Plating  means that the device is coated in copper at the top and the bottom.This is ideally for protection of the board , and ensures that the surfaces are durable and reliable. The coating can be tested through a tape test, which is used to check the adhesions of the surfaces finish, and the soldermarks.This test will strip a pressure -sensitive tape across  the surface and then pull it off sharply. There should be no bits of copper surface coating, solder masks for a correctly coated device.

**Specification(s)**
The weight of the pi board must not be too heavy <br />
The board weight limit is 17g.<br />

**Acceptance Test Criteria**
The weight of the board can be measured using a weight scale and it should not exceed 17g , otherwise the pi hat board might be too heavy and the raspberry zero board might not be able to sustain the weight density.



# **Electrical Specifications**

A board can only be called a HAT if:<br />
It conforms to the basic add-on board requirements <br />
It has a valid ID EEPROM (including vendor info, GPIO map and valid device tree information).  <br />
It has a full size 40W GPIO connector. <br />
It follows the HAT mechanical specification  <br />
If back-powering via the GPIO connector the HAT must be able to supply a minimum of 1.3A continuously to the Pi (but ability to supply 2A continuously is recommended).  <br />

**For the Power Regulator:**

The pi hat board must provide an additional 12 DC supply, which would be used as back up power and the current should not exceed a maximum of 2A and a minimum of 1.3 A <br />
BJT Transistor of npn  <br />
Output Voltage of approximately 5V  <br />

**For the LED Status Output:**

Use a timing controlling circuit , hence a 555 Timer  to control how long the LEDs will be on.  <br />
The output must source or sink 200mA  <br />
The temperature stability is 0.005% per C. <br />
The 555 timer  must operate in both astable and monostable mode <br />
To control the switching between the LEDs,  use a Timer IC of  IC CD4017 (Counter IC) and Diodes such as a IN4007 or IN4148 6Pcs

**For the Audio Amplifier**:

Use a standard universal op amp circuit to amplify the audio signals.<br />
The temperature should be kept at room temperature<br />
Output Voltage of 3.3 V

# **Detailed Electrical Specification with Testing**

**Specification(s)**
 The pi hat board should have GPIO Connectors <br />
 Board must have 40W GPIO connectors. <br />

**Acceptance Test Criteria**
The power of the GPIO pins can be measured using a multimeter instrument where one first measures the voltage and then measures the current, and then calculates the power using formula P=VI. It is important to include instrumental uncertainty and power factor.

**Specification(s)**
 The traffic robot system supplies additional power or back up power in case of emergency (main power) <br />
 The pi hat should supply a  maximum voltage of 5.25 V with a current of 5mA <br />

**Acceptance Test Criteria**
The voltage can be measured using a voltmeter. It is crucially to include the readings and instruments uncertainty to  ensure value does not exceed it’s limitations.

**Specification(s)**
The traffic robot system supplies additional power or back up power in case of emergency.(back-up power) <br />
The pi hat board must provide an additional 12 DC supply, which would be used as back up power.<br />
The current should not exceed a maximum of 2A <br />

**Acceptance Test Criteria**
The voltage can be measured using a voltmeter. It is crucially to include the readings and instruments uncertainty to  ensure value does not exceed it’s limitations.<br />
The current can be measured using an ammeter and it is important to ensure that the current does not exceed it’s limitations. 
R1.4: Be aware of the time length available to cross the road safely, before the smart robot switches it’s LED to a different colour.<br />

**Specification(s)**
Use a timing controlling circuit , hence a 555 Timer  to control how long the LEDs will be on. <br />
The output must source or sink 200mA<br />
The temperature stability is 0.005% per C. <br />
The 555 timer  must operate in both astable and monostable mode<br />

**Acceptance Test Criteria**
To test the Timer Circuit, one needs to connect the power supply to the 555 timer.If the 555 Timer is working properly, then the three LEDs should glow, if not then this means that the 555 timer is faulty.<br />

**Specification(s)**
Receive feedback from the system, hence green to Go or red to Stop.<br />
Receive feedback from the device, hence Red to Stop for pedestrian crossing.<br />
To control the switching between the LEDs,  use a Timer IC of  IC CD4017 (Counter IC) and a diodes such as a IN4007 or IN4148 6Pcs<br />

**Acceptance Test Criteria**
The Timer IC exists with the 555 Timer and can be tested in the same manner as the above test.

# **Functional Specifications with Testing**

**Specification(s)**
Project his/her voice to the smart traffic robot <br />
Use a differential op amp circuit to amplify the audio signals. <br />
Use a potentiometer  to control the volume (gain) <br />

**Acceptance Test Criteria**
The simplest way to test an op amp is by using an oscilloscope to measure the peak to peak amplitude of the output gain signal.The peak to peak amplitude of the input and output signal are compared, and hence there should be a gain of at least more than 10.<br />
For the potentiometer volume controller, it can be tested using a multimeter (measuring resistance) or an oscilloscope( comparing input and output).<br />

**Specification(s)**
Ensure that there is safety when crossing the road <br />
A built in 5 Megapixels, omniVision 0v5647Video must be installed within the system. <br />

**Acceptance Test Criteria**
To test if the camera is working, connect the camera sensor to your board which is connected to your PC. In the terminal line of your PC, execute the camera commands to check basic functionality.A terminal line in your PC can be used to examine the quality of the camera sensor in both picture and video format. <br />

**Specification(s)**
Device needs to eliminates all kinds of disturbance in the background <br />
Use a low and high passive circuit, hence a bandpass filter. <br />
Use a capacitor to filter noise.. <br />

**Acceptance Test Criteria**
Testing can be done using an oscilloscope, measured from the low frequency passband gain to the high frequency passband gain and as well as both the loss and amplitude response signal may be tested. The result should be in the expected range, hence attenuate low and high frequency,

**Specification(s)**
The system operates for 24 hours, 7 days a week<br />
Use an additional power supply , also known as backup power of a minimum of 12V.<br />

**Acceptance Test Criteria**
This can be measured using a voltmeter and it’s uncertainty and instrument rating should be taken into consideration.

**Specification(s)**
The system operates in all environment conditions <br />
Install a metal protection layer across the traffic robot system  to protect the system from any bad or extreme weather conditions. <br />

**Acceptance Test Criteria**
The durability and quality of the metal surface can be tested using the Indentation test and Scratch Hardness test technique.The indentation test can be measured in terms of depth of penetration in the material.Generally, as the material becomes softer, the depth of penetration increase and it fails the tests.The scratching test uses a Turner’s Sclerometer to slide against the surface of the material. The metal should not erode to pass the test.

**Specification(s)**
Monitor operation and performance of the smart traffic systems and roadcrossings <br />
Implement a camera sensor with a resolution of 1080p30 and a OmniVision Sensor. <br />
The sensor camera  uses a S/N ratio of 36 db. <br />
The focal focus is approximately 1m to infinity<br />
The focal length is 3,60 mm<br />
The focal ratio is 2.9.<br />

**Acceptance Test Criteria**
To test if the video is working, connect the camera sensor to your board which is connected to your PC. In the terminal line of your PC, execute the video commands to check it ‘s basic functionality.A terminal line in your PC can be used to examine the quality of the camera sensor in both picture and video format.This is to ensure that the video is working and traffic controllers are able to monitor the smart traffic lights at road crossing.

**Specification(s)**
The LEDs lights and brightness are working correctly<br />
Use 2.5 V Red LED. <br />
Use 2.2V Green LED.<br />
Use a 2.5V Purple LED.<br />

**Acceptance Test Criteria**
LED lights can be tested using a multimeter in diode mode and setting it to a regulator red LED. If the multimeter doesn’t change from 0L to open, then the device is broken.


#**Footprints Selection**	

Note!  When selecting footprint components, the following constraints were made:
The design used smaller size components to save money and space on board and reduced overall weight of the device.
Used coated surface with four layers of copper for durability  and protection reasons. Fours layers are more expensive than two, but four layers also guarantee protection and quality for a long period of time for components and board.
Surface Mount footprints were chosen over through hole components, because surface mount offers more security with an over protecting layer of solder.


# **Datasheet**

**555 Timer**
 
![alt text](https://3.bp.blogspot.com/-gEU4w3lz3pc/UD_ILJ7IMoI/AAAAAAAABUc/L4FdY-9_46o/s1600/555.JPGg)

**IC CD4017 decade counter**
 
![alt text](https://www.elprocus.com/wp-content/uploads/Circling-LEDs-Effect-212x300.jpg)


**Diodes of  IN4007 or IN4148 6Pcs**

![alt text](https://www.digchip.com/image-datasheet/161/1N4007.jpg)


**Universal Op Amp**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbesXsrJXuZ6ORKi_CdAlYVsjxDHK2ktkeCw&usqp=CAU)


**BJT Transistor**

![alt text](https://eng.libretexts.org/@api/deki/files/29171/clipboard_eded671b9989805a5866eea6454cb6dd2.png?revision=1)


