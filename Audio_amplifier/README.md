# Amplifier submodule


# Specifications:
1.Use a standard universal op amp circuit to amplify the audio signals. <br /> 
2.The temperature should be kept at room temperature no more than 37 degrees.<br /> 
3.Output Voltage of 3.3V. <br /> 

The Amplifier is intended to amplify the audio signal of the pi hat. Hence, the sub system picks up voice signals from the external environment. Then, the universal op amp will amplify the signal.In addition ,an additional capacitor is used in combination to the op amp to reduce and eliminate the noise of the signal.Eventually, the sub model will output a voltage signal of approximately 3.3V.

*This sub folder,consist of KICAD design and LTSPICE Stimulations of the system.*
