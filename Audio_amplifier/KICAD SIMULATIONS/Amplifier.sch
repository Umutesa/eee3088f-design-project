EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Amplifier by Umutesa Munyurangabo"
Date "2021-05-30"
Rev "01"
Comp "EEE3088F Design"
Comment1 "Amplifies an Audio Signal and filters noise signal"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small_US R1
U 1 1 60AF93C2
P 4450 3000
F 0 "R1" V 4655 3000 50  0000 C CNN
F 1 "1000" V 4564 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 4450 3000 50  0001 C CNN
F 3 "~" H 4450 3000 50  0001 C CNN
	1    4450 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R3
U 1 1 60AF9BED
P 5150 3000
F 0 "R3" V 5355 3000 50  0000 C CNN
F 1 "10000" V 5264 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 5150 3000 50  0001 C CNN
F 3 "~" H 5150 3000 50  0001 C CNN
	1    5150 3000
	0    -1   -1   0   
$EndComp
$Comp
L pspice:C C1
U 1 1 60AFB143
P 5700 3300
F 0 "C1" H 5878 3346 50  0000 L CNN
F 1 "100n" H 5878 3255 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5700 3300 50  0001 C CNN
F 3 "~" H 5700 3300 50  0001 C CNN
	1    5700 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2150 7400 2150
$Comp
L Device:R_Small_US R5
U 1 1 60B06A2A
P 7400 2500
F 0 "R5" H 7468 2546 50  0000 L CNN
F 1 "1000" H 7468 2455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 7400 2500 50  0001 C CNN
F 3 "~" H 7400 2500 50  0001 C CNN
	1    7400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 2150 7400 2400
Wire Wire Line
	7400 2600 7400 3050
$Comp
L power:GND #PWR010
U 1 1 60B08E0D
P 7400 3050
F 0 "#PWR010" H 7400 2800 50  0001 C CNN
F 1 "GND" H 7405 2877 50  0000 C CNN
F 2 "" H 7400 3050 50  0001 C CNN
F 3 "" H 7400 3050 50  0001 C CNN
	1    7400 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 60B0A145
P 4800 3350
F 0 "R2" H 4732 3304 50  0000 R CNN
F 1 "100" H 4732 3395 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 4800 3350 50  0001 C CNN
F 3 "~" H 4800 3350 50  0001 C CNN
	1    4800 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 3000 4800 3000
Wire Wire Line
	4800 3000 4800 3250
Connection ~ 4800 3000
Wire Wire Line
	4800 3000 5050 3000
Wire Wire Line
	5700 3000 5700 3050
$Comp
L power:GND #PWR07
U 1 1 60B0BDD4
P 5700 3550
F 0 "#PWR07" H 5700 3300 50  0001 C CNN
F 1 "GND" H 5705 3377 50  0000 C CNN
F 2 "" H 5700 3550 50  0001 C CNN
F 3 "" H 5700 3550 50  0001 C CNN
	1    5700 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3000 5700 3000
Connection ~ 5700 3000
Wire Wire Line
	5700 3000 6400 3000
Wire Wire Line
	6400 3000 6400 2350
Wire Wire Line
	6400 2350 6200 2350
Wire Wire Line
	7400 1650 7400 2150
Connection ~ 7400 2150
Wire Wire Line
	4800 3450 4800 3700
Text GLabel 4800 3700 3    50   Input ~ 0
Noise
Wire Wire Line
	5650 2250 5650 1650
Text GLabel 3550 4550 3    50   Output ~ 0
Noise
Wire Wire Line
	4350 3000 3700 3000
$Comp
L Simulation_SPICE:OPAMP U1
U 1 1 60B4D95B
P 5900 2250
F 0 "U1" H 5900 1769 50  0000 C CNN
F 1 "OPAMP" H 5900 1860 50  0000 C CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 5900 2250 50  0001 C CNN
F 3 "~" H 5900 2250 50  0001 C CNN
F 4 "Y" H 5900 2250 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "X" H 5900 2250 50  0001 L CNN "Spice_Primitive"
	1    5900 2250
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 60B587A7
P 6850 1650
F 0 "R4" H 6918 1696 50  0000 L CNN
F 1 "10000" H 6918 1605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 6850 1650 50  0001 C CNN
F 3 "~" H 6850 1650 50  0001 C CNN
	1    6850 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 1650 7400 1650
Wire Wire Line
	5650 1650 6750 1650
$Comp
L power:+3.3V #PWR08
U 1 1 60B6CE21
P 6000 1950
F 0 "#PWR08" H 6000 1800 50  0001 C CNN
F 1 "+3.3V" H 6015 2123 50  0000 C CNN
F 2 "" H 6000 1950 50  0001 C CNN
F 3 "" H 6000 1950 50  0001 C CNN
	1    6000 1950
	1    0    0    -1  
$EndComp
$Comp
L power:-3V3 #PWR09
U 1 1 60B6E9D8
P 6000 2600
F 0 "#PWR09" H 6000 2700 50  0001 C CNN
F 1 "-3V3" H 6015 2773 50  0000 C CNN
F 2 "" H 6000 2600 50  0001 C CNN
F 3 "" H 6000 2600 50  0001 C CNN
	1    6000 2600
	-1   0    0    1   
$EndComp
$Comp
L Simulation_SPICE:VTRNOISE V1
U 1 1 60BA365C
P 3550 3900
F 0 "V1" H 3680 3991 50  0000 L CNN
F 1 "VTRNOISE" H 3680 3900 50  0000 L CNN
F 2 "Sensor_Audio:ST_HLGA-6_3.76x4.72mm_P1.65mm" H 3550 3900 50  0001 C CNN
F 3 "~" H 3550 3900 50  0001 C CNN
F 4 "Y" H 3550 3900 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 3550 3900 50  0001 L CNN "Spice_Primitive"
F 6 "trnoise(20n 0.5n 0 0)" H 3680 3809 50  0000 L CNN "Spice_Model"
	1    3550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 4100 3550 4550
$Comp
L power:+5V #PWR06
U 1 1 60BA4F13
P 3550 3700
F 0 "#PWR06" H 3550 3550 50  0001 C CNN
F 1 "+5V" H 3565 3873 50  0000 C CNN
F 2 "" H 3550 3700 50  0001 C CNN
F 3 "" H 3550 3700 50  0001 C CNN
	1    3550 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2250 4650 2250
Wire Wire Line
	6000 2550 6000 2600
Text HLabel 4650 2250 0    50   Output ~ 0
P3.3V
Text HLabel 3700 3000 0    50   Input ~ 0
P5V
Text Notes 8400 6800 0    118  ~ 24
Audio Amplifier
Text Notes 4250 2150 0    118  Italic 24
Output
Text Notes 3450 2850 0    118  Italic 24
Input
Text Notes 4550 4150 0    118  Italic 24
Noise Input
$EndSCHEMATC
