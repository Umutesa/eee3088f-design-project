# **EEE3088F Design Project**

**Name :** Smart Traffic Robot System with Voice Applications

# **Description**
Our System aims to limit the number of Covid-19 infections, since there is a possibility that South Afica
might experience the third wave of Covid-19 infections within a few weeks. This system is mainly designed for pedestrians wanting to cross the roadway, without having to physically touch the device. The system will allow a pedestrian to project his/her voice to the traffic robot device, and in turn makes an indication to the user when it is safe to cross the road.

# **Installation**
The project requires two softwares:
1. LTspice software, which may be installed from ltspice.en.softonic.com
2. KiCad software which may be installed from https://www.kicad.org

# **Outstanding Issues in the Project Design**
1. The power regulatory circuitry does not regulated to the expected 5V. As a result oftemperature and inefficiency of the BJT transistor the output was not an ideal of 5 V, but an approximation of 4.2 V.

2. The amplifier does not output a 3,3V voltage reading as expected. The output voltage is limited to a maximum of 3 V.This is due to irregularities of the op amp such as temperature, input impedance, etc and the noise signal, hence a higher noise signal attenuates the output signal.

3. For the LED Status output, the operation of the error/purple LED did not turn out as expected from the simulation. The LED is supposed to turn on only when there is an error rather than every time the green LED turns off and the red LED is about to turn on. This issue can be resolved with a suitable program when the raspberry pi is set up and programmed.

4. PCB Design has electrical wiring issues when peforming ERC Test in KICAD. However, these issue are not a great concern
(mainly because of poor wiring layout), but design still operates efficiently.

# **Acknowledgment**
The raspberry pi hat template board for PCB was used from an external source 
Credits : https://github.com/rkprojects/kicad-rpiz-uhat-template

## **License**
This project is licensed under GNU Affero General Public License v3.0 - see LICENSE for details.
