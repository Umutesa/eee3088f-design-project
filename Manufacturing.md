# **Manufacturing**

This repository contains content communications on how to use sub models for other projects including Bill of Materials.

# **How to use the Amplifier sub-system?**

**Step 1** : Connect Audio signal to the input

![alt text](https://qph.fs.quoracdn.net/main-qimg-9cea7d92d5fa941e1b1cd735068e661d)

**Step 2** : The noise will be detected from the Audio Signal

![alt text](https://i1.wp.com/static.dailymirror.lk/media/images/image_1546966880-1aa12d694c.jpg)

**Step 3** : This system reduces the noise input and amplifies an output Audio

![alt text](https://soundsightheadphones.com/wp-content/uploads/2020/06/Sound-Waves-Cancelling-Noise-Source-vs-Anti-Noise.jpg)

**Step 4** : Enjoy listening to the Sound.

![alt text](https://www.videomaker.com/wp-content/uploads/2018/09/380-C04-Audio-Primary-696x392.jpg)

# **How to use the LED Status subsystem?**
**Step 1** : Connect sub system to the input supply

![alt text](https://www.circuitspecialists.com/blog/wp-content/uploads/2020/01/61Aq0U9tvGL._SL1500_-1-1024x573.jpg)

**Step 2** : Order Instructions to System.Hence “STOP” to output RED LED, “GO” to output a green LED and third LED will only be output if signal can’t be understood

![alt text](https://www.allaboutcircuits.com/uploads/articles/trafficlightJPG.jpg)

**Step 3** : The LED’S should change output at different instructions.

![alt text](https://media.istockphoto.com/photos/traffic-lights-over-urban-intersection-picture-id1168877485?k=6&m=1168877485&s=612x612&w=0&h=LJMMQ8sF9s5bRiI2tNsEqYYv56L0exuK8Od7FDJ9qg4=)

# **How to install the raspberry template?**

**KiCad Board Template For Raspberry Pi Zero (W) uHAT**

Easy starting point for Raspberry Pi Zero (W) uHAT extension board. GPIO 40 Pin connector is of through hole type, board size is 65x30mm. Pi Zero or Zero W boards do not have PoE header, other components can be placed in that region.
ID EEPROM is not added as its footprint package and I2C pull-up resistors size could be different based on your project.
Please refer to HAT Design Guidelines 
KiCad version: 5.0.2

**Installing as template in KiCad on Ubuntu**

If KICAD_USER_TEMPLATE_DIR environment variable is not set:
Open KiCAD > Preferences Menu > Configure Paths
Set KICAD_USER_TEMPLATE_DIR environment variable to a directory where you would create/copy all user templates.
Get design files
$ git clone https://github.com/rkprojects/kicad-rpiz-uhat-template.git
$ cd kicad-rpiz-uhat-template
Copy to template directory
$ cp -r raspberrypi_zerow_uhat <kicad_user_template_dir_path_set_above>
Or create symbolic link
$ ln -sr raspberrypi_zerow_uhat <kicad_user_template_dir_path_set_above>
Using Template
Create New project from template.
Open KiCad > Press CTRL-T or from Menu File > New > Project from Template
Click User Templates tab > Click Raspberry Pi icon > If there are many icons read template information to cross check > Click OK button.
Refer to KiCad docs for more details.


# **How to install KICAD?**

The KiCad schematic library and docs are in separate packages. If you want either of these install kicad-library and kicad-doc respectively.
If you prefer to use the shell, you can enter these commands into a terminal:
sudo add-apt-repository --yes ppa:kicad/kicad-5.1-releases sudo apt update sudo apt install --install-recommends kicad # If you want demo projects sudo apt install kicad-demos
This will perform a full installation of KiCad. If you don’t want to install all packages you can use:

sudo apt install --no-install-recommends kicad

If you don’t do a "full installation" you should be interested to install also:

kicad-libraries: a virtual package that will recommend you to install footprints, symbols, templates and 3D
kicad-symbols, kicad-templates, kicad-footprints, kicad-packages3d: if you want to manually select what library to install.
Additional packages can be also installed according your language:
kicad-locale-XX: locales for your language (XX is your country code. see apt search kicad-locale to find available languages)
kicad-doc-XX: documentation in your language. Same remark than kicad-locale-XX

**Installation**

KiCad 5.0.2 is available in PPA for KiCad: 5.0 releases.
To install KiCad via the PPA, you can use the Software Manager:
Open the Software Manager.
Select 'Edit' → 'Software Sources…​'.
Open the 'Other Software' tab.

Click 'Add…​', and enter the PPA address: ppa:js-reynaud/kicad-5 and then click the 'Add Source' button.
When prompted insert the administrator user password.
Return to the Software Manager and view the Progress tab to see when the cache has finished updating.
Return to the Software Manager main screen, search for 'kicad', and install it.
The KiCad schematic library and docs are in separate packages. If you want either of these install kicad-library and kicad-doc respectively.

If you prefer to use the shell, you can enter these commands into a terminal:

sudo add-apt-repository --yes ppa:js-reynaud/kicad-5 sudo apt update sudo apt install kicad --install-recommends kicad # If you want demo projects sudo apt install kicad-demos
This will perform a full installation of KiCad. If you don’t want to install all packages you can use:
sudo apt install --no-install-recommends kicad

If you don’t do a "full installation" you should be interested to install also:
kicad-libraries: a virtual package that will recommend you to install footprints, symbols, templates and 3D
kicad-symbols, kicad-templates, kicad-footprints, kicad-packages3d: if you want to manually select what library to install.
kicad-locale-XX: locales for your language (XX is your con-try code. see apt search kicad-locale to find available languages)
kicad-doc-XX: documentation in your language. Same remark than kicad-locale-XX

**Acknowledgments**

Install on Ubuntu link : https://www.kicad.org/download/ubuntu/

# **How to install LTSPICE?**

**Install Wine and other dependencies**

Most probably you know how to install Wine in your Linux distro, but I'm going to repeat the same thing again !
To install Wine using apt in Debian or other Debian based distros like Ubuntu, Linux Mint, use the following instructions.
If you're on a 64 bit installation, then you've to enable i386 repository, it may be necessary to run some parts of Wine properly.

First open up a terminal emulator, then run the commands one by one, listed below.

sudo dpkg --add-architecture i386            
Add the i386 architecture
sudo apt-get update
sudo apt-get install wine wine-mono0.0.8 wine-gecko2.21
Don't forget to check the proper wine-mono and wine-gecko version.

**Install LTspice Linux**
Now you've to download the LTspice version XVII, get it from here.
Usually double clicking the exe file will start the Wine program installer interface.

**Acknowledgments**

Installing LTSPICE : https://www.pcsuggest.com/install-ltspice-linux/


# **Bill of Materials (BOM)**

**General equipment**

**Raspberry Pi hat**

Things you need in general: <br />

A Raspberry Pi Zero Board <br />
A USB male connector <br />
Breadboard jumper wires <br />
GPIO header connectors <br />
1 x male 2x20 Pin Headers <br />
1 x female 2x20 Pin Headers <br />
1 x male 1x4 Pin Header  <br />

**PSU**

This list of materials is to build the power regulator. <br />
1 x Voltage supply via USB 12 V <br />
1 x resistor 1kΩ <br />
1 x Capacitor 100mF <br />
1 x zener diode 5V <br />
1 x resistor 150Ω <br />
1 x transistor npn (BJT) <br />

**Amplifier**

This list of materials is to build the audio amplifier. <br />
1 x universal standard op amp <br />
3 x resistor 1kΩ <br />
1 x Capacitor 100nF <br />
1 x resistor 10kΩ <br />
1 x resistor 100Ω <br />

**Led Status**

This list of materials is to build the LED Status. <br />
3 x LED 1.4 V <br />
6 x Diode 0.7 V <br />
3 x resistor 1kΩ <br />
1 x 555 Timer <br />
1 x IC  <br />
1 x Capacitor 47nF <br />
1 x Capacitor 10uF <br />
1 x resistor 150kΩ <br />
1 x resistor 620kΩ <br />

**Cost of Materials** 

All components can be purchased from  https://www.robotics.org.za/
It is also guaranteed that you will not likely spend more than R1000 on entire project. 

**Miscellaneous**
A Raspberry Pi Zero+ (or a newer one with the same layout may be used)
5m USB 2.0 cables (don't need to be the best quality when it comes to data transfer speed, we only use USB for convenience reasons and to transfer the push signal)
