# **Smart Traffic Robot System with Voice Applications**

This repository contains everything you need to know in using the system build and connecting them with your software. This system is intended to minimise the covid -19 infection ,  by using “smart” internet of things approach.

![alt text](https://nakedsecurity.sophos.com/wp-content/uploads/sites/2/2018/03/shutterstock_604059146.jpg)

**Table of Contents** <br />
How we build and run them <br />
What we need / Bill of materials (BOM) <br />
Specifications <br />
PSU <br />
Amplifier <br />
LED Status <br />
The Result <br />
Raspberry Pi Board <br />
PCB Design <br />
Applications of System <br />


# **How we build and run them**
The system is a raspberry pi hat expansion board that connects to the raspberry board.The raspberry pi hat has been built independently  of the raspberry zero board.The pi hat allows additional functionality to the board, such as light , sensors, etc.In our case, the model provides the additional functionality of audio voice signals.<br />
The pi hat consists of  three sub models.The power regulator module which allows the device to receive additional power via USB.The audio amplifier which amplifies the voice signal and the LED status output which outputs the desired feedback required.<br />
When a pedestrian commands “STOP” to the system, the device will receive the signal , amplify it and output a red LED light to the traffic device.<br />
When a pedestrian commands “GO” to the system, the device will receive the signal , amplify it and output a green LED light to the traffic device.<br />
Any other noise or voice signal will not be identified by the device.Hence, it will output a purple LED light for error notification.<br />

# **What we need / Bill of materials (BOM)**
To replicate this project and build your model, you need a couple of things. We have created a complete list of what you need, a so-called Bill of material (BOM).<br />
Check it out:  [Bill of materials (BOM)](https://gitlab.com/Umutesa/eee3088f-design-project/-/blob/main/Manufacturing.md#bill-of-materials-bom)

# **PSU Specifications**

To replicate this project and build your model, you need the following specifications for the power regulator.<br />
Check it out:  [PSU Specifications](https://gitlab.com/Umutesa/eee3088f-design-project/-/blob/main/ReferenceManual.md#electrical-specifications)

# **Amplifier Specifications**
To replicate this project and build your model, you need the following specifications for the amplifier. <br />
Check it out:  [Amplifier Specifications](https://gitlab.com/Umutesa/eee3088f-design-project/-/blob/main/ReferenceManual.md#electrical-specifications)

# **LED Status Specifications**

To replicate this project and build your model, you need the following specifications for the power regulator. <br />
Check it out:  [LED Specifications](https://gitlab.com/Umutesa/eee3088f-design-project/-/blob/main/ReferenceManual.md#electrical-specifications)

# **The result**
Simulations for each system were recorded using LTSPICE software. <br />

To install LTSPICE , follow :
[LTSPICE](https://gitlab.com/Umutesa/eee3088f-design-project/-/blob/main/Manufacturing.md#how-to-install-ltspice)
[KICAD](https://gitlab.com/Umutesa/eee3088f-design-project/-/blob/main/Manufacturing.md#how-to-install-kicad)

# **Custom Raspberry Pi case**

A custom already designed KICAD template was used. <br />
Have a look at :

[Install Raspberry template](https://gitlab.com/Umutesa/eee3088f-design-project/-/blob/main/Manufacturing.md#how-to-install-the-raspberry-template)

# **PCB Design**

The final design was constructed in KICAD. <br />
To install KICAD , follow :   <br />
Check out design: [PCB Design](https://gitlab.com/Umutesa/eee3088f-design-project/-/tree/main/Raspberry_microHat)

# **Applications**
At road Crossings <br />

![alt text](https://image.freepik.com/free-vector/vehicles-with-infrared-sensor-device-unmanned-smart-cars-city-traffic-vector-illustration_53562-4067.jpg)
