# Status LEDs submodule

# Specifications:

Uses a 555 timer to time the state of the LEDs <br /> 
The output must source or sink 200mA <br /> 
To control the switching between the LEDs, it uses an IC CD4017 decade counter <br /> 
Diodes such as a IN4007 or IN4148 6Pcs <br /> 

The device makes use of 3 LEDs to indicate the status of the pedestrian traffic lights which are used to guide the user(s) trying to cross the road. The LEDs each have their unique designated colours to show the different states of the pedestrian traffic lights. This device uses red, green, and purple LEDs. The red LED indicates that it is not yet safe for the pedestrian to cross the road. The green LED indicates that the user can now cross the road, and when an error occurs, the purple LED is used to indicate this.
These LEDs turn on one at a time, no two or more LEDs can switch on simultaneously. The state of these LEDs is mainly dependent on an external user input that is in the form of a voice command that is received by the device, telling it that a user desires to cross the road. After the user command has been decoded into machine-appropriate data that can be used by the device, an input signal is transferred into a 555 timer to generate clock pulses as an output. The generated output pulses are inputted into an IC-4017 decade counter that is used for time synchronization purposes. The counter is connected to the LEDs and it turns the LEDs ON or OFF depending on the state of the input pulses.
When the input is high, the green LED is turned on and when it is low, the red LED is turned on. The blue LED is turned on in the case that an error has occurred whether due to system failure or an incorrect/uninterpretable user command given over the audio mics.

*This sub folder contains LTSPICE simulations and KICAD schematic of this device.*
