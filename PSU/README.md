# Power Supply submodule

# Specifications:

The pi hat board must provide an additional 12V DC supply (via USB Cable) , which would be used as back up power and the current should not exceed a maximum of 2A and a minimum of 1.3 A <br /> 
BJT Transistor (NPN Transistor) <br /> 
Output Voltage of approximately 5V <br /> 

*This sub folder consist of KICAD and LTSPICE Simulations of the design.*
