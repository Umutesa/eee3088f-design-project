EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "PSU by Brendon Mutema"
Date "2021-05-30"
Rev "01"
Comp "EEE3088F Design"
Comment1 "The circuit gets input of at least 12 V and reguates it to 5V"
Comment2 "The 12V can be supplied via USB cable or PC DC Supply"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery VSupply1
U 1 1 60B3325A
P 4800 3350
F 0 "VSupply1" H 4908 3396 50  0000 L CNN
F 1 "12V" H 4908 3305 50  0000 L CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" V 4800 3410 50  0001 C CNN
F 3 "~" V 4800 3410 50  0001 C CNN
	1    4800 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R6
U 1 1 60B3465D
P 5300 3250
F 0 "R6" V 5095 3250 50  0000 C CNN
F 1 "150" V 5186 3250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 5300 3250 50  0001 C CNN
F 3 "~" H 5300 3250 50  0001 C CNN
	1    5300 3250
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener ZD1
U 1 1 60B36370
P 5600 3500
F 0 "ZD1" V 5554 3580 50  0000 L CNN
F 1 "5V" V 5645 3580 50  0000 L CNN
F 2 "Battery:Battery_Panasonic_CR1025-VSK_Vertical_CircularHoles" H 5600 3500 50  0001 C CNN
F 3 "~" H 5600 3500 50  0001 C CNN
	1    5600 3500
	0    1    1    0   
$EndComp
$Comp
L Device:CP1_Small C2
U 1 1 60B375CB
P 6050 3250
F 0 "C2" H 6141 3296 50  0000 L CNN
F 1 "100microF" V 5950 3050 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 6050 3250 50  0001 C CNN
F 3 "~" H 6050 3250 50  0001 C CNN
	1    6050 3250
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC548 Q1
U 1 1 60B388BB
P 5600 2950
F 0 "Q1" V 5928 2950 50  0000 C CNN
F 1 "BC548" V 5837 2950 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5800 2875 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC550-D.pdf" H 5600 2950 50  0001 L CNN
	1    5600 2950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US RLoad1
U 1 1 60B3CB20
P 6450 3250
F 0 "RLoad1" H 6518 3296 50  0000 L CNN
F 1 "1K" H 6518 3205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 6450 3250 50  0001 C CNN
F 3 "~" H 6450 3250 50  0001 C CNN
	1    6450 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 60B3E7D1
P 5600 3750
F 0 "#PWR011" H 5600 3500 50  0001 C CNN
F 1 "GND" H 5605 3577 50  0000 C CNN
F 2 "" H 5600 3750 50  0001 C CNN
F 3 "" H 5600 3750 50  0001 C CNN
	1    5600 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3150 4800 2850
Wire Wire Line
	4800 2850 5050 2850
Wire Wire Line
	5050 2850 5050 3250
Wire Wire Line
	5050 3250 5200 3250
Connection ~ 5050 2850
Wire Wire Line
	5050 2850 5400 2850
Wire Wire Line
	5600 3150 5600 3250
Wire Wire Line
	5400 3250 5600 3250
Connection ~ 5600 3250
Wire Wire Line
	5600 3250 5600 3350
Wire Wire Line
	4800 3550 4800 3750
Wire Wire Line
	5600 3750 5600 3650
Wire Wire Line
	5800 2850 6050 2850
Wire Wire Line
	6050 2850 6050 3150
Wire Wire Line
	5600 3750 6050 3750
Wire Wire Line
	6050 3750 6050 3350
Connection ~ 5600 3750
Wire Wire Line
	6050 2850 6450 2850
Connection ~ 6050 2850
Wire Wire Line
	6050 3750 6450 3750
Connection ~ 6050 3750
Wire Wire Line
	4800 3750 5600 3750
Wire Wire Line
	6450 3350 6450 3750
Wire Wire Line
	6450 2850 6450 3150
Text Notes 8300 6750 0    118  ~ 24
Power Regulator (PSU)\n
Connection ~ 6450 2850
Wire Wire Line
	6450 2850 6600 2850
Text HLabel 6600 2850 2    50   Output ~ 0
P5V
Text Notes 6950 2850 0    118  Italic 24
Output
$EndSCHEMATC
